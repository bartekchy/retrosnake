﻿# RetroSnake

**RetroSnake** jest klonem gry [Snake](
https://en.wikipedia.org/wiki/Snake_%28video_game_genre%29) wydanej po raz
pierwszy w 1976 roku pod nazwą [Blockade](
https://en.wikipedia.org/wiki/Blockade_%28video_game%29). Silnik gry napisany
został w języku JavaScript i wykorzystuje on do wyświetlania grafiki element
[canvas](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/canvas)
dodany w HTML5.

Gra w finalnej wersji będzie pozwalała na konfigurację:
* prędkości poruszania się węża,
* planszy, na której toczy się rozgrywka,
* wyświetlanego stylu graficznego.

## Wdrożenie

Do wdrożenia projektu używane są Dockerowe kontenery.
Poniżej znajduje się lista komend użytecznych przy wdrażaniu:

* budowanie obrazu kontenera zawierającego projekt:  
  `docker build -t retrosnake .`
* Uruchomienie kontenera z projektem dla potrzeb debugowania:
  `docker run -it -p 80:80 retrosnake bash`
* Uruchomienie kontenera z projektem dla potrzeb testowania:  
  `docker run -p 80:80 retrosnake`
* Zapisanie obrazu kontenera do pliku:
  `docker save -o retrosnake.tar retrosnake`
* Wczytanie obrazu kontenera z pliku:
  `docker load -i retrosnake.tar`  
