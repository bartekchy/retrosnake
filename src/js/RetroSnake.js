
class RetroSnake {
  constructor(canvasId) {
    this.ended = false;
    this.curKey = 0;
    this.started = false;
    this.lastCurKey = 0;
    this.pauseCurKey = 0;
    this.continues = true;
    this.appTime = 0;
    this.appLasTime = 0;
    this.pauseTime = 0;
    this.stopDate = 0;
    this.interval = null;
    this.canvas = document.getElementById(canvasId),
    this.context = this.canvas.getContext('2d');
    this.cols = 15;
    this.rows = 15;
    this.walls = 0;
    this.intervalTime = 300;
    this.maxApples = 4;
    this.appleDelay = 2000;
    this.appleTime = 10000;
    this.gameElem = {
        GROUND: 'ground',
        WALL:   'wall',
        APPLE:  'apple',
        SNAKE:  'snake'
    }
    this.apples = [];
    this.snakeEl = [
        {x:1, y:1, ck: 0, lck: 0},
        {x:1, y:1, ck: 0, lck: 0},
        {x:1, y:1, ck: 0, lck: 0}
    ];
    this.areaLen = new Array(this.cols);
    for (var i = 0; i < this.areaLen.length; i++) {
      this.areaLen[i] = new Array(this.rows);
      for (var j = 0; j < this.areaLen[i].length; j++){
      if(i == 0 || j == 0 || i == this.cols -1 || j == this.rows -1){
        this.areaLen[i][j] = this.gameElem.WALL;
        this.walls++;
       }
      else
        this.areaLen[i][j] = this.gameElem.GROUND;
      }
    }
    this.areaElem = this.rows * this.cols - this.walls;
  }
  restart(){
  this.ended = false;
  this.curKey = 0;
  this.started = true;
  this.lastCurKey = 0;
  this.pauseCurKey = 0;
  this.continues = true;
  this.appTime = 0;
  this.appLasTime = 0;
  this.pauseTime = 0;
  this.stopDate = 0;
  this.interval = null;
  this.apples = [];
  this.snakeEl = [
    {x:1, y:1, ck: 0, lck: 0},
    {x:1, y:1, ck: 0, lck: 0},
    {x:1, y:1, ck: 0, lck: 0}
  ];
  for (var i = 0; i < this.areaLen.length; i++) {
     this.areaLen[i] = new Array(this.rows);
     for (var j = 0; j < this.areaLen[i].length; j++){
        if(i == 0 || j == 0 || i == this.cols -1 || j == this.rows -1){
        this.areaLen[i][j] = this.gameElem.WALL;
        this.walls++;
     }
        else
        this.areaLen[i][j] = this.gameElem.GROUND;
     }
  }


  }

  run(){
    this.menu();
    var parent = this;
    window.addEventListener('keydown', function(){parent.keyDownListener()});
  }
  menu(){
     var height = this.canvas.height;
     var width = this.canvas.width;
     var parent = this;
     var image = new Image();
     image.src = 'js/img/start.png';
     image.onload = function(){
        parent.context.drawImage(image, 0, 0, image.width, image.height, 0, 0, height, width);
        parent.context.fillStyle = 'rgba(0, 0, 0, 0.6)';
        parent.context.fillRect(105, 50, 300, 390);
        parent.context.font = "50px Arial";
            parent.context.textAlign = "center";
            parent.context.fillStyle = "#ffffff";
            parent.context.fillText("RetroSnake", width/2, height/5);
            parent.context.font = "20px Arial";
            parent.context.textAlign = "left";
            parent.context.fillText("Press space to start", width/3, 150);
            parent.context.fillText("←  move left", width/3, 200);
            parent.context.fillText(" ↑   move up", width/3, 240);
            parent.context.fillText("→  move right", width/3, 280);
            parent.context.fillText(" ↓   move down", width/3, 320);
            parent.context.fillText(" p    pause", width/3, 360);
            parent.context.fillText("esc quit game", width/3, 400);

            parent.context.rect(0, 0, width, height);
            parent.context.stroke();

     }

  }

  start(){
    if(this.interval == null){
        var parent = this;
        this.interval = setInterval(function(){parent.step()}, this.intervalTime);
    }
}

  step() {
    this.apple();
    this.snake();
    this.calcCell();
    for(var i = 0; i < this.cols; i++){
        for(var j = 0; j < this.rows; j++){
            switch (this.areaLen[i][j]) {
              case this.gameElem.WALL:
                this.drawCell(i, j, 'js/img/wall.png')
                break;
              case this.gameElem.GROUND:
                this.drawCell(i, j, 'js/img/ground.png')
                break;
              case this.gameElem.APPLE:
                this.drawCell(i, j, 'js/img/apple.png')
                break;
              case this.gameElem.SNAKE:
                  if(i == this.snakeEl[this.snakeEl.length-1].x && j == this.snakeEl[this.snakeEl.length-1].y){
                    this.drawCell(i, j, 'js/img/head.png')
                    break;
                  }
                  for(var x = 0; x < this.snakeEl.length - 1; x++){
                    if(i == this.snakeEl[x].x && j == this.snakeEl[x].y)
                    this.drawSnake(i, j, this.snakeEl[x+1].ck , this.snakeEl[x+1].lck);
                  }
                break;
              }
        }
    }
  }

  stop() {
    if(this.interval != null){
        clearInterval(this.interval);
        this.interval = null;
    }
    if(!this.continues){
    var parent = this;
      var x = setTimeout(function(){parent.pauseView()}, 100);
    }
  }
  pauseView(){
    this.context.fillStyle = 'rgba(0, 0, 0, 0.6)';
            this.context.fillRect(105, 160, 300, 150);
            this.context.font = "50px Arial";
                this.context.textAlign = "center";
                this.context.fillStyle = "#ffffff";
                this.context.fillText("PAUSE", 255, 220);
                this.context.font = "15px Arial";
                this.context.fillText("Press p to resume", 255, 260);
                this.context.fillText("Press escape to quit game", 255, 285);
  }

  keyDownListener(){
    switch (event.key) {
      case "ArrowDown":
        if(this.curKey == 1 || this.lastCurKey == 1)
            break;
        this.curKey = 0;
        break;
      case "ArrowUp":
        if(this.curKey == 0 || this.lastCurKey == 0)
            break;
        this.curKey = 1;
        break;
      case "ArrowLeft":
        if(this.curKey == 3 || this.lastCurKey == 3)
            break;
        this.curKey = 2;
        break;
      case "ArrowRight":
        if(this.curKey == 2 || this.lastCurKey == 2)
            break;
        this.curKey = 3;
        break;
      case "p":
      if(this.started && !this.ended){
         if(this.continues){
            this.continues = false;
             this.stop();
            this.stopDate = Date.now();
            this.pauseCurKey = this.curKey;
         }else{
            this.start();
            this.continues = true;
            this.pauseTime += (Date.now() - this.stopDate);
            this.curKey = this.pauseCurKey;
         }
       }
        break;
      case ' ':
        if(!this.started){
            this.start();
            this.started = true;
            this.curKey = 0;
        }
        if(this.ended){
            this.restart();
            this.start();
        }
        break;
      case "Escape":
        this.stop();
        this.restart();
        this.started = false;
        var parent = this;
        var x = setTimeout(function(){parent.menu()}, 100);


        break;
      default:
        return;
    }
  }

  drawCell(col, row, path){
    var x = col * this.colsize;
    var y = row * this.rowsize;
    var parent = this;
    var image = new Image();
    image.src = path;
    image.onload = function(){
        parent.context.drawImage(image, 0, 0, image.width, image.height, x, y, parent.colsize, parent.rowsize);
    }
  }
  calcCell(){
    this.colsize = this.canvas.width / this.cols;
    this.rowsize = this.canvas.height / this.rows;
  }
  apple(){
        var x,y;
        this.appTime = Date.now();
        var diff = this.appTime - this.appLasTime - this.pauseTime;
        if(diff > this.appleDelay || this.apples.length == 0){
            do {
                x = Math.floor(Math.random() * (Math.floor(this.cols -1) + 1));
                y = Math.floor(Math.random() * (Math.floor(this.rows -1) + 1));
            } while(this.areaLen[x][y] != this.gameElem.GROUND);

            this.apples.push({x:x, y:y, time: this.appTime});
            this.areaLen[x][y] = this.gameElem.APPLE;
            this.appLasTime = Date.now();
        }
        diff = this.appTime - this.apples[0].time - this.pauseTime;
        if(this.apples.length > this.maxApples || diff > this.appleTime){
            var o = this.apples[0];
            this.areaLen[o.x][o.y] = this.gameElem.GROUND;
            this.apples.shift();
         }
  }
  snake(){
    var x = this.snakeEl[0].x;
    var y = this.snakeEl[0].y;
    var parent = this;
    var headX = this.snakeEl[this.snakeEl.length-1].x;
    var headY = this.snakeEl[this.snakeEl.length-1].y;
    this.areaLen[x][y] = this.gameElem.GROUND;

    switch (this.curKey) {
          case 0:
            headY++;
            break;
          case 1:
            headY--;
            break;
          case 2:
            headX--;
            break;
          case 3:
            headX++;
            break;
          default:
            return;
    }
    this.snakeEl.push({x:headX, y:headY, ck:this.curKey, lck: this.lastCurKey});
    if(this.areaLen[headX][headY] == this.gameElem.WALL || this.areaLen[headX][headY] == this.gameElem.SNAKE){
       this.areaLen[x][y] = this.gameElem.SNAKE;
       this.ended = true;
       this.stop();
       var x = setTimeout(function(){parent.drawOver()}, 100);

    }else if (this.areaLen[headX][headY] == this.gameElem.APPLE){
        for(var i = 0; i < this.apples.length; i++){
            if(this.apples[i].x == headX && this.apples[i].y == headY)
            this.apples.splice(i,1);
         }

        this.areaLen[headX][headY] = this.gameElem.SNAKE;
        this.areaLen[x][y] = this.gameElem.SNAKE;

        if (this.snakeEl.length == this.areaElem){
            this.ended = true;
            this.stop();
            var x = setTimeout(function(){parent.drawWin()}, 100);
        }

    }else{
        this.areaLen[headX][headY] = this.gameElem.SNAKE;
        this.snakeEl.shift();
    }
  }
  drawOver(){
    this.context.fillStyle = 'rgba(0, 0, 0, 0.6)';
            this.context.fillRect(95, 160, 320, 150);
            this.context.font = "50px Arial";
                this.context.textAlign = "center";
                this.context.fillStyle = "#ffffff";
                this.context.fillText("GAME OVER", 255, 220);
                this.context.font = "15px Arial";
                this.context.fillText("Press space to restart", 255, 260);
                this.context.fillText("Press escape to quit game", 255, 285);
  }
  drawWin(){
        this.context.fillStyle = 'rgba(0, 0, 0, 0.6)';
              this.context.fillRect(105, 160, 300, 150);
              this.context.font = "50px Arial";
                  this.context.textAlign = "center";
                  this.context.fillStyle = "#ffffff";
                  this.context.fillText("YOU WIN!!!", 255, 220);
                  this.context.font = "15px Arial";
                  this.context.fillText("Press space to restart", 255, 260);
                  this.context.fillText("Press escape to quit game", 255, 285);
    }

  drawSnake(i, j, ck, jck){
    switch(ck) {
        case 0:
            if(jck == 0)
                this.drawCell(i, j, 'js/img/snakeUD.png');
            if(jck == 2)
                this.drawCell(i, j, 'js/img/snakeDR.png');
            if(jck == 3)
                this.drawCell(i, j, 'js/img/snakeDL.png');
            break;
        case 1:
            if(jck == 1)
                this.drawCell(i, j, 'js/img/snakeUD.png');
            if(jck == 2)
                this.drawCell(i, j, 'js/img/snakeUR.png');
            if(jck == 3)
                this.drawCell(i, j, 'js/img/snakeUL.png');
            break;
        case 2:
            if(jck == 2)
                this.drawCell(i, j, 'js/img/snakeRL.png');
            if(jck == 1)
                this.drawCell(i, j, 'js/img/snakeDL.png');
            if(jck == 0)
                this.drawCell(i, j, 'js/img/snakeUL.png');
            break;
        case 3:
            if(jck == 3)
                this.drawCell(i, j, 'js/img/snakeRL.png');
            if(jck == 1)
                this.drawCell(i, j, 'js/img/snakeDR.png');
            if(jck == 0)
                this.drawCell(i, j, 'js/img/snakeUR.png');
            break;
    }
    this.lastCurKey = this.curKey;
  }
}
