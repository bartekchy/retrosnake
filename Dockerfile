FROM ubuntu:18.04
EXPOSE 80/tcp
EXPOSE 443/tcp
RUN apt-get -y update
RUN apt-get -y install nginx
COPY ./src /var/www/html
CMD ["nginx", "-g", "daemon off;"]
